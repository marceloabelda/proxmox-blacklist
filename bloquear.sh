#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat << EOF # remove the space between << and EOF, this is due to web plugin issue
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Script description here.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-f, --flag      Some flag description
-p, --param     Some param description
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  tipo=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -t | --tipo) # tipo dominio o regexp
      tipo="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ -z "${tipo-}" ]] && die "Missing required parameter: tipo"
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

parse_params "$@"
setup_colors

# script logic here

msg "${RED}Read parameters:${NOFORMAT}"
msg "- tipo: ${tipo}"
msg "- arguments: ${args[*]-}"



recorrer_mxs() {
	# keys
	keys_array=("litoralcaws.pem" "mx1.dilfer.ar.pem" "palmaresltd.pem" "vpc-jost.pem")
	mxs_array=("mx1.litoralcitrus.ar" "mx2.dilfer.ar" "mx1.palmares.ar" "mx4.jost.com.ar")

	for indice in "${!keys_array[@]}"
	do
    		key="${keys_array[$indice]}"
    		mx="${mxs_array[$indice]}"
    		echo "Key  $indice: $key"
    		echo "MX  $indice: $mx"

		xfull_cmd="ssh -i ~/keys/$key admin@$mx  -t $comando \" "
		echo "Listo MX $mx $xfull_cmd"
		full_cmd=$(ssh -i ~/keys/$key admin@$mx  -t $comando \")
		echo $full_cmd 
	done
}


val=$args
cmd_create="\"sudo pmgsh create /config/ruledb/who/2"
cmd_final=" &> /dev/null &\""


case $tipo in
  ip)
      comando="$cmd_create/ip --ip $val"
      recorrer_mxs
      ;;
  em)
    comando="$cmd_create/email --email $val"
      recorrer_mxs
      ;;
  dom)
     comando="$cmd_create/domain --domain $val"
      recorrer_mxs
      ;;
  sdom)
    comando="$cmd_create/regex --regex  '.+\.$val'"
      recorrer_mxs
      ;;
  net)
     comando="$cmd_create/network --cidr $val"
      recorrer_mxs
      ;;
  
  *)
  echo "Los valores validos de tipo son ip, net, em, dom o sdom";;
  esac
